<?php

$jsonData = file_get_contents('https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum-report/data.json');
$jsonData1 = json_decode($jsonData, true);

$year = [];
$petroleum_product = [];
$sale = [];
$country = [];

$db = new SQLite3('petroleum_products.db');
//Deleting table if it already exists.
//        $db->exec("DROP TABLE IF EXISTS petroleum;");
//Creating the table if table does not exists.
$db->exec("CREATE TABLE IF NOT EXISTS petroleum(year INT,  petroleum_product VARCHAR(255), sale INT, country VARCHAR(255))");

foreach ($jsonData1 as $key => $value) {
    $year = $value['year'];
    $petroleum_product = $value['petroleum_product'];
    $sale = $value['sale'];
    $country = $value['country'];
    //    Fetching the data for petroleumDataTable from json_data.
    $data = $db->prepare("INSERT INTO petroleum(year, petroleum_product, sale, country) VALUES(?, ?, ?, ?)");
    $data->bindParam(1, $year);
    $data->bindParam(2, $petroleum_product);
    $data->bindParam(3, $sale);
    $data->bindParam(4, $country);
    $data->execute();
}
$extracted_data1 = $db->query("SELECT country, petroleum_product, SUM(sale) AS all_sale FROM petroleum GROUP BY country, petroleum_product");
$extracted_data2 = $db->query("SELECT product1.petroleum_product, product1.year||'-'||product2.year as Year, (sale1+sale2)/2 as avg_sale FROM 
        ((SELECT petroleum_product, year, SUM(sale) AS sale1 FROM petroleum GROUP BY petroleum_product, year) AS product1 JOIN (SELECT petroleum_product, year, SUM(sale) AS sale2 FROM petroleum GROUP BY petroleum_product, year) AS product2 ON 
        product2.year == product1.year + 1 AND product1.petroleum_product == product2.petroleum_product) WHERE product2.year%2==0");
$extracted_data3 = $db->query("SELECT petroleum_product, year, MIN (all_sale) AS min_sale FROM (SELECT petroleum_product, year, SUM(sale) AS all_sale FROM petroleum GROUP BY petroleum_product, year) WHERE all_sale > 0 GROUP BY petroleum_product");
?>
<!DOCTYPE html>
<html>
<head>
   <title>YIPL Intership challenge.</title>

   <style>
      table {
         font-family: arial, sans-serif;
         border-collapse: collapse;
         width: 50%;
      }
      td, th {
         border: 1px solid #dddddd;
         text-align: left;
         padding: 8px;
      }
      tr:nth-child(even) {
         background-color: #dddddd;
      }
   </style>
</head>

<body>
<h2>1. Listing overall sale of each petroleum product by country.</h2>
<table>
   <thead>
   <tr>
      <th>Country</th>
      <th>Product</th>
      <th>Over-all Sale</th>
   </tr>
   </thead>
   <tbody>
   <?php
   while ($row1 = $extracted_data1->fetchArray()){
   ?>
   <tr>
      <td><?php echo "{$row1['country']}" ?></td>
      <td><?php echo "{$row1['petroleum_product']}" ?></td>
      <td><?php echo "{$row1['all_sale']}" ?></td>
   </tr>
  <?php } ?>
   </tbody>
</table>
<hr>

<h2>2. Average sale of each petroleum Product for 2 years of interval.</h2>
<table>
   <thead>
   <tr>
      <th>Product</th>
      <th>Year</th>
      <th>Average-Sale</th>
   </tr>
   </thead>
   <tbody>
   <?php
   while ($row2 = $extracted_data2->fetchArray()){
       ?>
      <tr>
         <td><?php echo "{$row2['petroleum_product']}" ?></td>
         <td><?php echo "{$row2['Year']}" ?></td>
         <td><?php echo "{$row2['avg_sale']}" ?></td>
      </tr>
   <?php } ?>
   </tbody>
</table>
<hr>

<h2>3. Year when the petroleum product had least sale.</h2>
<table>
   <thead>
   <tr>
      <th>Product</th>
      <th>Year</th>
      <th>Least-Sale</th>
   </tr>
   </thead>
   <tbody>
   <?php
   while ($row3 = $extracted_data3->fetchArray()){
       ?>
      <tr>
         <td><?php echo "{$row3['petroleum_product']}" ?></td>
         <td><?php echo "{$row3['year']}" ?></td>
         <td><?php echo "{$row3['min_sale']}" ?></td>
      </tr>
   <?php } ?>
   </tbody>
</table>
</body>
</html>

