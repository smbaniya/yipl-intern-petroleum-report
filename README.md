# README #

Necessary steps for web-appplication to be running in web-browsers.

#Prerequisite and Description

* VERSION: PHP  8.0.2 version used. 
* Pull the repo called “yipl-intern-petroleum-report” from bitbucket.
* Petroleum Report problem was solved and pushed in private repo with commits in bitbucket with proper message.
* internship-bitbucket@yipl.com.np is invited to “yipl-intern-petroleum-report” repo.

# Steps for web-application to be running in web browser.

* First Download XAMPP from [https://www.apachefriends.org/download.html] and install it in you system.
* After Completion of the installation. you can use the XAMPP Control panel to start/stop all servers.
* Start Apache Server.
* Download SQLite DB Browser to store the data [https://sqlitebrowser.org/dl/].
* To enable SQLITE with PHP [https://www.php.net/manual/en/book.sqlite3.php] (follow the steps to enable SQLITE with PHP.)
* Copy report.php to htdocs (File Path: C:/Program Files/XAMPP/htdocs) Or Where the htdocs file is present.
* Now to run your code, open localhost/report.php in any web-browser, then it gets executed.

